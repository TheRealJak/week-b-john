package Cars;

public class Car {

	private String brand;
	private int speed;
	private int maxSpeed;
	private boolean isOn;
	private String color;


	public Car(String b, int s, int ms, boolean io, String c){

		brand = b;

		speed = s;

		isOn = io;

		color = c;

		maxSpeed = ms;

	}
	public String getBrand(){

		return brand;

	}
	public int getSpeed(){

		return speed;

	}
	public int getMaxSpeed(){

		return maxSpeed;

	}
	public boolean getIsItOn(){

		return isOn;

	}
	public String getColor(){

		return color;

	}
	public void setBrand(String b){

		brand = b;

	}
	public void accelerate(int s){

		if(speed <= maxSpeed  && isOn == true){
			
			speed += s;
		}

	}
	public void decelerate(int s){

		if(speed >= 5 && isOn == true){
			
			speed -= s;
		}

	}
	public void setMaxSpeed(int ms){

		maxSpeed = ms;

	}
	public void turnOn(boolean io){

		io = true;

		isOn = io;

	}
	public void turnOff(boolean io){

		io = false;

		isOn = io;

	}
	public void setColor(String c){

		color = c;

	}

}
