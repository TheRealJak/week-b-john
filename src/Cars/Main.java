package Cars;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		Car ford = new Car("ford", 0, 50, false, "blue");

		while(true){

			System.out.println("Enter what you would like to know about your car : ");

			String demandedInfo = scan.next();

			if(demandedInfo.equals("brand")){

				System.out.println(ford.getBrand());

			}

			else if(demandedInfo.equals("speed")){

				System.out.println(ford.getSpeed());

			}
			else if(demandedInfo.equals("max speed")){

				System.out.println(ford.getMaxSpeed());

			}
			else if(demandedInfo.equals("is it on")){
				
				if(ford.getIsItOn() == true){
				
					System.out.println("Yes");
				
				}
				else if(ford.getIsItOn() == false){
					
					System.out.println("No");
				
				}

			}
			else if(demandedInfo.equals("color")){

				System.out.println(ford.getColor());

			}

		}
	}



}


