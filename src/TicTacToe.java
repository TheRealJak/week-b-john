import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		boolean GameRunning = true;

		boolean Player1Turn = true;

		boolean Player2Turn = false;
		
		int Rotations = 1;
		
		Scanner scan = new Scanner(System.in);

		String[][] board = new String[3][3];

		board[0][0] = "[ ] ";
		board[0][1] = "[ ] ";
		board[0][2] = "[ ] ";
		board[1][0] = "[ ] ";
		board[1][1] = "[ ] ";
		board[1][2] = "[ ] ";
		board[2][0] = "[ ] ";
		board[2][1] = "[ ] ";
		board[2][2] = "[ ] ";

		while(GameRunning == true){
			
			for (int i = 0; i < 3; i++){

				System.out.println(" ");

				for (int j = 0; j < 3; j++){

					System.out.print(board[i][j]);

				}
			}


			//System.out.println("Would you like to be X or O : ");
			//String Player1Type = scan.next();

			if(Player1Turn == true){

				System.out.println(" ");
				System.out.println("Enter an x coordinate : ");
				int Player1ResponseX = scan.nextInt();

				System.out.println("Enter an y coordinate : ");
				int Player1ResponseY = scan.nextInt();

				board[Player1ResponseY - 1][Player1ResponseX - 1] = "[X] ";

				Player1Turn = false;
				Player2Turn = true;

			}
			else{

				System.out.println(" ");
				System.out.println("Enter an x coordinate : ");
				int Player2ResponseX = scan.nextInt();

				System.out.println("Enter an y coordinate : ");
				int Player2ResponseY = scan.nextInt();

				if(board[Player2ResponseY - 1][Player2ResponseX - 1].equals ("[ ] ")){

					board[Player2ResponseY - 1][Player2ResponseX - 1] = "[O] ";

					Player1Turn = true;
					Player2Turn = false;
				}
				else if(board[Player2ResponseY - 1][Player2ResponseX - 1].equals ("[ ] ")){

					board[Player2ResponseY - 1][Player2ResponseX - 1] = "[X] ";

					Player1Turn = false;
					Player2Turn = true;

				}
				else{

					System.out.println("Error try again.");

				}

			}

			if(board[0][0].equals("[X] ") && board[0][1].equals("[X] ") && board[0][2].equals("[X] ")){

				System.out.println("X wins.");
				GameRunning = false;

			}
			else if(board[1][0].equals("[X] ") && board[1][1].equals("[X] ") && board[1][2].equals("[X] ")){

				System.out.println("X wins.");
				GameRunning = false;

			}
			else if(board[2][0].equals("[X] ") && board[2][1].equals("[X] ") && board[2][2].equals("[X] ")){

				System.out.println("X wins.");
				GameRunning = false;

			}
			else if(board[0][0].equals("[X] ") && board[1][0].equals("[X] ") && board[2][0].equals("[X] ")){

				System.out.println("X wins.");
				GameRunning = false;

			}
			else if(board[0][1].equals("[X] ") && board[1][1].equals("[X] ") && board[2][1].equals("[X] ")){

				System.out.println("X wins.");
				GameRunning = false;

			}
			else if(board[0][2].equals("[X] ") && board[1][2].equals("[X] ") && board[2][2].equals("[X] ")){

				System.out.println("X wins.");
				GameRunning = false;

			}
			else if(board[0][0].equals("[X] ") && board[1][1].equals("[X] ") && board[2][2].equals("[X] ")){

				System.out.println("X wins.");
				GameRunning = false;

			}
			else if(board[0][2].equals("[X] ") && board[1][1].equals("[X] ") && board[2][0].equals("[X] ")){

				System.out.println("X wins.");
				GameRunning = false;

			}
			else if(board[0][0].equals("[O] ") && board[0][1].equals("[O] ") && board[0][2].equals("[O] ")){

				System.out.println("O wins.");
				GameRunning = false;

			}
			else if(board[1][0].equals("[O] ") && board[1][1].equals("[O] ") && board[1][2].equals("[O] ")){

				System.out.println("O wins.");
				GameRunning = false;

			}
			else if(board[2][0].equals("[O] ") && board[2][1].equals("[O] ") && board[2][2].equals("[O] ")){

				System.out.println("O wins.");
				GameRunning = false;

			}
			else if(board[0][0].equals("[O] ") && board[1][0].equals("[O] ") && board[2][0].equals("[O] ")){

				System.out.println("O wins.");
				GameRunning = false;

			}
			else if(board[0][1].equals("[O] ") && board[1][1].equals("[O] ") && board[2][1].equals("[O] ")){

				System.out.println("O wins.");
				GameRunning = false;

			}
			else if(board[0][2].equals("[O] ") && board[1][2].equals("[O] ") && board[2][2].equals("[O] ")){

				System.out.println("O wins.");
				GameRunning = false;

			}
			else if(board[0][0].equals("[O] ") && board[1][1].equals("[O] ") && board[2][2].equals("[O] ")){

				System.out.println("O wins.");
				GameRunning = false;

			}
			else if(board[0][2].equals("[O] ") && board[1][1].equals("[O] ") && board[2][0].equals("[O] ")){

				System.out.println("O wins.");
				GameRunning = false;

			}
			else if (Rotations == 9){
				System.out.println("Tie.");
				GameRunning = false;
			}
			Rotations++;

		}
	}

}
