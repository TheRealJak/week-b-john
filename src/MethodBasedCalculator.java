import java.util.Scanner;

public class MethodBasedCalculator {

	public static int add(int number, int number2){
		
		return number + number2;
		
	}
	public static int subtract(int number , int number2){
		
		return number - number2;
		
	}
	public static int multiply(int number , int number2){
		
		return number * number2;
		
	}
	public static int divide(int number , int number2){
		
		return number / number2;
		
	}
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter a number : ");
		
		int FirstNumber = scan.nextInt();
		
		System.out.print("Enter operation(+ for addition - for subtration * for multiplication and / for division) : ");
		
		String operation = scan.next();
		
		System.out.println("Enter another number : ");
		
		int SecondNumber = scan.nextInt();
		
		if(operation.equals("+")){

			System.out.println(add(FirstNumber, SecondNumber));

		}
		else if(operation.equals("-")){

			System.out.println(subtract(FirstNumber, SecondNumber));

		}
		else if(operation.equals("*")){

			System.out.println(multiply(FirstNumber, SecondNumber));

		}
		else if(operation.equals("/")){

			System.out.println(divide(FirstNumber, SecondNumber));

		}
		else{

			System.out.println("Invalid operation.");

		}
		
	}
	
}
