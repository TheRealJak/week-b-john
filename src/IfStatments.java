import java.util.Scanner;

public class IfStatments {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner (System.in);
		
		int number = scan.nextInt();
		
		if(1 == number){
			
			System.out.println("The variable equals 1");
			
		}
		else{
			
			System.out.println("The variable != 1.");
			
		}

	}

}
